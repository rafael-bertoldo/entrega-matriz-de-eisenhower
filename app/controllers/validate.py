def classification(urgency, importance):
    if urgency == 1 and importance == 1:
        return 1
    elif urgency == 2 and importance == 1:
        return 2
    elif urgency == 1 and importance == 2:
        return 3
    else:
        return 4


def verify_eisenhower_class(item):
    if item == 1:
        return 'Do It First'
    elif item == 2:
        return 'Schedule It'
    elif item == 3:
        return 'Delegate It'
    else:
        return "Delete It"

def validate_keys(request_value, categorie):

    correct_keys = {
        "categorie": {"categories_description"},
        "task": {"tasks_importance", "tasks_urgency", "tasks_description"}
    }

    keys = set(request_value.keys())

    if len(keys) == 0:
        return {"Available keys": list(correct_keys[categorie])}, 400

    else:
        return True