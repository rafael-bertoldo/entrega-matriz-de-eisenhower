from flask import json, jsonify, request, current_app
import sqlalchemy
import psycopg2
from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm.exc import UnmappedInstanceError
from app.models.categories_model import CategoriesModel
from .validate import validate_keys


def list_category():
    list_all = CategoriesModel.query.all()

    return jsonify(list_all), 200



def insert_categorie():

    session = current_app.db.session

    try:

        data = request.get_json()

        categorie = CategoriesModel(**data)

        session.add(categorie)
        session.commit()

        new_categorie = dict(categorie)
        del new_categorie['tasks']
        return jsonify(new_categorie)

    except IntegrityError:

        return {"msg": "category already exists"}, 409


def delete_categorie(categorie_id: int):

    session = current_app.db.session

    try:

        categorie = CategoriesModel.query.get(categorie_id)

        session.delete(categorie)
        session.commit()

        return jsonify(categorie), 204

    except UnmappedInstanceError:

        return {"msg": "category not found"}, 404



def update_categorie(categorie_id):

    session = current_app.db.session

    data = request.get_json()

    filtered_data = CategoriesModel.query.get(categorie_id)

    if filtered_data is None:
        return {"msg": "category not found"}

    try:
        validate_keys(data, "categorie")

        for key, value in data.items():
            setattr(filtered_data, key, value)

            session.add(filtered_data)
            session.commit()

            base_for_return = CategoriesModel.query.filter_by(categories_id = categorie_id).first()
            
            returnable = {
                "id": base_for_return.categories_id,
                "categories_name": base_for_return.categories_name,
                "categories_description": base_for_return.categories_description
                }

    except KeyError:
        return {"error" : "Wrong key", "Available Key": "categories_description"}, 400
    

    return jsonify(returnable), 200