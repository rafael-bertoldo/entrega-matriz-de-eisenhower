from flask import jsonify, request, current_app
from app.models.categories_model import CategoriesModel
from app.models.tasks_model import TasksModel
from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm.exc import UnmappedInstanceError
from .validate import classification, validate_keys, verify_eisenhower_class


def create_task():
    try:

        data = request.get_json()

        importance_validate = True
        urgency_validate = True

        if data['importance'] < 1 or data['importance'] > 2:
            importance_validate = False

        if data['urgency'] < 1 or data['urgency'] > 2:
            urgency_validate = False

        if importance_validate == False or urgency_validate == False:

            return {
                "error": {
                    "valide options": {
                        "importance": [
                            1,
                            2
                        ],

                        "urgency": [
                            1,
                            2
                        ]
                    },

                    "received": {
                        "importance": data['importance'],
                        "urgency": data['urgency']
                    }
                }
            }, 404

        else:

            correct_data = {
                "tasks_name": data['name'],
                "tasks_description": data['description'],
                "tasks_duration": data['duration'],
                "tasks_importance": data['importance'],
                "tasks_urgency": data['urgency'],
                "eisenhower_id": classification(data['urgency'], data['importance'])
            }

            task = TasksModel(**correct_data)

            current_app.db.session.add(task)
            current_app.db.session.commit()

            for key in data['categories']:
                category = CategoriesModel.query.filter_by(categories_name=key['categories_name']).first()
                
                if category is None:
                    breakpoint()
                    new_categorie = CategoriesModel(**key)
                    new_categorie.tasks.append(task)
                    current_app.db.session.add(new_categorie)
                    current_app.db.session.commit()
                else:
                    category.tasks.append(task)
                    current_app.db.session.add(category)
                    current_app.db.session.commit()

            returnable = {
                "id": task.tasks_id,
                "name": task.tasks_name,
                "description": task.tasks_description,
                "duration": task.tasks_duration,
                "eisenhower_classification": verify_eisenhower_class(task.eisenhower_id),
                "categories": [key for key in data['categories']]
            }

            return jsonify(returnable)
    
    except IntegrityError:
     return {"msg": "task already exists"}, 409



def delete_task(task_id: int):

    try:

        task = TasksModel.query.get(task_id)

        current_app.db.session.delete(task)
        current_app.db.session.commit()

        return jsonify(task), 204

    except UnmappedInstanceError:

        return {"msg": "task not found"}, 404


def update_task(task_id):

    session = current_app.db.session

    data = request.get_json()

    filtered_data = TasksModel.query.get(task_id)


    if filtered_data is None:
        return {"msg": "task not found"}

    try:
        validate_keys(data, "task")

        for key, value in data.items():
            setattr(filtered_data, key, value)

            session.add(filtered_data)
            session.commit()

            base_for_return = TasksModel.query.filter_by(tasks_id = task_id).first()
            
            returnable = {
                "id": base_for_return.tasks_id,
                "tasks_name": base_for_return.tasks_name,
                "tasks_description": base_for_return.tasks_description,
                "tasks_duration": base_for_return.tasks_duration,
                "eisenhower_classification": verify_eisenhower_class(base_for_return.eisenhower_id)
                }

    except KeyError:
        return {"error" : "Wrong key", "Available Key": "tasks_description"}, 400
    

    return jsonify(returnable), 200