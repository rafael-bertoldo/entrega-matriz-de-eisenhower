from flask import Blueprint
from app.controllers.tasks_controller import create_task, delete_task,  update_task

bp = Blueprint('tasks_bp', __name__, url_prefix='tasks')

bp.post('')(create_task)
bp.patch('/<task_id>')(update_task)
bp.delete('/<task_id>')(delete_task)