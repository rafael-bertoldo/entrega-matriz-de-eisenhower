from flask import Blueprint
from app.controllers.category_controller import delete_categorie, insert_categorie, update_categorie

bp = Blueprint('categories_bp', __name__, url_prefix='category')

bp.post('')(insert_categorie)
bp.delete('/<categorie_id>')(delete_categorie)
bp.patch('/<categorie_id>')(update_categorie)
