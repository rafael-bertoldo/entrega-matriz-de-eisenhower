from . import  category_blueprint, tasks_blueprint
from flask import Blueprint
from app.controllers.category_controller import list_category

bp = Blueprint('api_bp', __name__, url_prefix='/api')

bp.register_blueprint(category_blueprint.bp)
bp.register_blueprint(tasks_blueprint.bp)

bp.get('')(list_category)