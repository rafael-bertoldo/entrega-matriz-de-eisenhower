from flask import Flask
from flask_migrate import Migrate

def init_app(app: Flask):

    from app.models.eisenhower_model import EisenhowerModel
    from app.models.tasks_model import TasksModel
    from app.models.categories_model import CategoriesModel
    from app.models.tasks_categories_model import TasksCategoriesModel

    Migrate(app, app.db)