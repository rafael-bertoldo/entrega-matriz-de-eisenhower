from app.configs.database import db
from dataclasses import dataclass

@dataclass
class EisenhowerModel(db.Model):

    eisen_id: int
    eisen_type: str

    __tablename__ = 'eisenhowers'

    eisen_id = db.Column(db.Integer, primary_key=True)
    eisen_type = db.Column(db.String(100), nullable=False)