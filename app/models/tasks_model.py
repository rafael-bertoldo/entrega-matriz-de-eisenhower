from app.configs.database import db
from dataclasses import dataclass
from sqlalchemy.orm import backref, relationship

@dataclass
class TasksModel(db.Model):

    tasks_id: int
    tasks_name: str
    tasks_description: str
    tasks_duration: int
    tasks_importance: int
    tasks_urgency: int
    eisenhower_id: int

    __tablename__ = 'tasks'

    tasks_id = db.Column(db.Integer, primary_key=True)
    tasks_name = db.Column(db.String(100), nullable=True, unique=True)
    tasks_description = db.Column(db.String())
    tasks_duration = db.Column(db.Integer)
    tasks_importance = db.Column(db.Integer)
    tasks_urgency = db.Column(db.Integer)
    eisenhower_id = db.Column(db.Integer, db.ForeignKey('eisenhowers.eisen_id'))

    def __iter__(self):
        yield "tasks_id", self.tasks_id,
        yield "tasks_name", self.tasks_name,
        yield "tasks_description", self.tasks_description,
        yield "tasks_duration", self.tasks_duration,
        yield "tasks_importance", self.tasks_importance

