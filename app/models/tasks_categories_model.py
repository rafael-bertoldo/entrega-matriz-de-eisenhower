from sqlalchemy.orm import backref, relationship
from app.configs.database import db
from dataclasses import dataclass

@dataclass
class TasksCategoriesModel(db.Model):

    tasks_category_id: int
    tasks_id:int


    __tablename__ = 'tasks_categories'

    tasks_category_id = db.Column(db.Integer, primary_key=True)
    tasks_id = db.Column(db.Integer, db.ForeignKey('tasks.tasks_id'))
    categorie_id = db.Column(db.Integer, db.ForeignKey('categories.categories_id'))
