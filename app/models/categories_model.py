from app.configs.database import db
from dataclasses import dataclass
from sqlalchemy.orm import backref, relationship

@dataclass
class CategoriesModel(db.Model):

    categories_id: int
    categories_name: str
    categories_description: str
    tasks: list

    __tablename__ = 'categories'

    categories_id = db.Column(db.Integer, primary_key=True)
    categories_name = db.Column(db.String(100), nullable=False, unique=True)
    categories_description = db.Column(db.String)

    tasks = relationship('TasksModel', secondary='tasks_categories', backref='categories')

    def __iter__(self):
        yield "categories_id", self.categories_id,
        yield "categories_name", self.categories_name,
        yield "categories_description", self.categories_description,
        yield "tasks", self.tasks
